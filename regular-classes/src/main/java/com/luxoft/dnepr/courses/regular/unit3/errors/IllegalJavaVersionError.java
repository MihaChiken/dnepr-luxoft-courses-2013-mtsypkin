package com.luxoft.dnepr.courses.regular.unit3.errors;

public class IllegalJavaVersionError extends Error {
    private String actualJavaVersion;
    private String expectedJavaVersion;

    public IllegalJavaVersionError(String inpActualJavaVersion, String inpExpectedJavaVersion, String message) {
        super(message);
        actualJavaVersion = inpActualJavaVersion;
        expectedJavaVersion = inpExpectedJavaVersion;
    }

    public String getActualJavaVersion(){
        return actualJavaVersion;
    }

    public String getExpectedJavaVersion(){
        return expectedJavaVersion;
    }
}
