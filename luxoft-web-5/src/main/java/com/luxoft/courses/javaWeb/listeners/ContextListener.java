package com.luxoft.courses.javaWeb.listeners;

import com.luxoft.courses.javaWeb.Constants;
import com.luxoft.courses.javaWeb.CurrentStatistics;
import com.luxoft.courses.javaWeb.Users;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener{
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute(
                Constants.ACTIVE_SESSION_ATTRIBUTE, new CurrentStatistics());
        sce.getServletContext().setAttribute(
                Constants.USERS_STORAGE_ATTRIBUTE,
                new Users(sce.getServletContext().getInitParameter("users")));
    }
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute(
                Constants.ACTIVE_SESSION_ATTRIBUTE);
        sce.getServletContext().removeAttribute(
                Constants.USERS_STORAGE_ATTRIBUTE);
    }
}
