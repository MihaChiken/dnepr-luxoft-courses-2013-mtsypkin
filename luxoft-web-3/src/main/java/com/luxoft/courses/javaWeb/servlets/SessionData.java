package com.luxoft.courses.javaWeb.servlets;

import com.luxoft.courses.javaWeb.CurrentStatistics;
import com.luxoft.courses.javaWeb.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class SessionData extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        CurrentStatistics statistics =((CurrentStatistics) getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE));
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Session data</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h2>Session data:</h2>");
        out.println("<table border='1'>" +
                "<tr>" +
                "<td>Active Sessions</td>" +
                "<td>"+(statistics.getUserSessions().get()+statistics.getAdminSessions().get())+"</td>" +
                "</tr>" +
                "<tr>" +
                "<td>Active Sessions(ROLE user)</td>" +
                "<td>"+statistics.getUserSessions()+"</td>" +
                "</tr>" +
                "<tr>" +
                "<td>Active Sessions(ROLE admin)</td>" +
                "<td>"+statistics.getAdminSessions()+"</td>" +
                "</tr>" +

                "<tr>" +
                "<td>Total Count of HttpRequest</td>" +
                "<td>"+(statistics.getGetRequests().get()+statistics.getPostRequests().get()+statistics.getOtherRequests().get())+"</td>" +
                "</tr>" +
                "<tr>" +
                "<td>Total Count of POST HttpRequest</td>" +
                "<td>"+statistics.getPostRequests()+"</td>" +
                "</tr>" +
                "<tr>" +
                "<td>Total Count of GET HttpRequest</td>" +
                "<td>"+statistics.getGetRequests()+"</td>" +
                "</tr>" +
                "<tr>" +
                "<td>Total Count of Other HttpRequest</td>" +
                "<td>"+statistics.getOtherRequests()+"</td>" +
                "</tr>" +
                "</table>");
        out.println("</body>");
        out.println("</html>");
    }
}
