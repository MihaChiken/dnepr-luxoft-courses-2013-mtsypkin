package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class WalletIsBlockedException extends Exception {
    private Long walletId;
    public WalletIsBlockedException(Long inpWalletId, String message){
        super(message);
        walletId = inpWalletId;
    }

    public Long getWalletId(){
        return walletId;
    }

}
