package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Wallet implements  WalletInterface{
    private long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    private String format(BigDecimal decimal){
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        DecimalFormatSymbols custom = new DecimalFormatSymbols();
        custom.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(custom);
        return df.format(decimal);
    }
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public WalletStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if(status == WalletStatus.BLOCKED)
            throw  new  WalletIsBlockedException(id, "Wallet is blocked");
        if(amount.compareTo(amountToWithdraw) == -1)
            throw new  InsufficientWalletAmountException(id, amount, amountToWithdraw, "Insufficient wallet amount ("+format(amount)+" < "+format(amountToWithdraw)+")");

    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        amount = amount.subtract(amountToWithdraw);
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if(status == WalletStatus.BLOCKED)
            throw  new  WalletIsBlockedException(id, "Wallet is blocked");
        if(amountToTransfer.add(amount).compareTo(maxAmount) == 1)
            throw new  LimitExceededException(id, amount, amountToTransfer, "Wallet limit exceeded ("+format(maxAmount)+" < "+format(amount)+" + "+format(amountToTransfer)+")");
    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
        amount = amount.add(amountToTransfer);
    }
}
