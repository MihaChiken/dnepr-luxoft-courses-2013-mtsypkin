package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct implements Product {
    private boolean nonAlcoholic;

    Beverage(String inpCode, String inpName, double inpPrice, boolean inpNonAlcoholic) {
        setName(inpName);
        setCode(inpCode);
        setPrice(inpPrice);
        nonAlcoholic = inpNonAlcoholic;
    }

    public boolean isNonAlcoholic() {
        return nonAlcoholic;
    }

    public void setNonAlcoholic(boolean isNoneAlcoholic) {
        nonAlcoholic = isNoneAlcoholic;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode() + (nonAlcoholic ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass())
            return false;

        return super.equals(obj) && ((Beverage) obj).nonAlcoholic == this.nonAlcoholic;
    }
    @Override
    public Object clone() throws CloneNotSupportedException {
        Beverage obj = (Beverage) super.clone();
        obj.setNonAlcoholic(nonAlcoholic);
        return obj;
    }
}
