package com.luxoft.courses.javaWeb.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;


public class LoginForm extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Please sign in</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<form action='/getLogin' method='post'>" +
                "Login:<br>" +
                "<input type='text' name='login'><br>" +
                "Password:<br>" +
                "<input type='password' name='password'><br>" +
                "<input type='submit' name='signIn' value='Sign in'>" +
                "</form>");
        Map<String, String[]> params = request.getParameterMap();
        if (params.containsKey("invalid")) {
                out.println("<p>Wrong login or password</p>");
        }
        out.println("</body>");
        out.println("</html>");
    }
}
