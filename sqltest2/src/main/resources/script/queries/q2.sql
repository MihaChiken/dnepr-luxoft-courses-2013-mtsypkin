SELECT
  laptop.speed,
  makers.maker_name
FROM laptop, product, makers
WHERE makers.maker_id = product.maker_id AND
      product.model = laptop.model AND
      laptop.hd >= 10