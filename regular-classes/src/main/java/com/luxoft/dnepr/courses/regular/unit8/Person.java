package com.luxoft.dnepr.courses.regular.unit8;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Person implements Externalizable {

    private String name;
    private Gender gender;
    private String ethnicity;
    private Date birthDate;
    private Person father;
    private Person mother;


    public String toStringJSON() {
        StringBuilder textToWrite = new StringBuilder("");
        textToWrite.delete(0, textToWrite.length());
        textToWrite.append("{\"name\":\"");
        textToWrite.append(getName());
        textToWrite.append("\",\"gender\":\"");
        textToWrite.append(getGender().toString());
        textToWrite.append("\",\"ethnicity\":\"");
        textToWrite.append(getEthnicity());
        textToWrite.append("\",\"birthDate\":\"");
        textToWrite.append(getBirthDate().getTime());
        textToWrite.append("\"");

        if (father != null) {
            textToWrite.append(",\"father\":");
            textToWrite.append(father.toStringJSON());

        }

        if (mother != null) {
            textToWrite.append(",\"mother\":");
            textToWrite.append(mother.toStringJSON());
        }
        textToWrite.append("}");

        return textToWrite.toString();
    }

    private static List<String> splitByLevel(String text, int level) {
        List<String> result = new ArrayList<>();
        int currentLevel = 0, start = 2;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == '{') {
                currentLevel++;
            }
            if (text.charAt(i) == '}') {
                currentLevel--;
            }
            if (text.charAt(i) == ',' && (text.charAt(i - 1) == '\"' || text.charAt(i - 1) == '}') && text.charAt(i + 1) == '\"' && currentLevel == level) {
                result.add(text.substring(start, i - 1));
                start = i + 2;
            }
        }
        result.add(text.substring(start, text.length() - 1));
        return result;
    }
    private String cutName(String text){
        int i;
        for(i=0;i < text.length() && text.charAt(i)!=':';i++){}
        return text.substring(i,text.length());
    }
    private String getStringFromBraces(String text){
        int start,end;
        for(start=0;start < text.length() && text.charAt(start)!='\"';start++){}
        for(end=start+1;end < text.length() && text.charAt(end)!='\"';end++){}
        return text.substring(start+1,end);
    }
    private void savePairs(String text) {
        text = cutName(text);
        List<String> pairs = splitByLevel(text, 1);
        String[] nameAndValue = pairs.get(0).split(":");
        name = getStringFromBraces(nameAndValue[1]);

        nameAndValue = pairs.get(1).split(":");
        if (nameAndValue[1].equals("\"FEMALE\"")) {
            gender = Gender.FEMALE;
        } else {
            gender = Gender.MALE;
        }

        nameAndValue = pairs.get(2).split(":");
        ethnicity = nameAndValue[1].substring(1,nameAndValue[1].length());

        nameAndValue = pairs.get(3).split(":");
        birthDate= new Date();
        String date = nameAndValue[1].substring(1, nameAndValue[1].length());
        if(date.charAt(date.length()-1)=='}')
            date = date.substring(0,date.length()-2);
        birthDate.setTime(Long.parseLong(date));

        if(pairs.size() == 5){
            nameAndValue = pairs.get(4).split(":");
            if(nameAndValue[0].equals("father\"")){
                father = new Person();
                father.savePairs(pairs.get(4));
            } else {
                mother = new Person();
                mother.savePairs(pairs.get(4));
            }
        }
        if (pairs.size() == 6){
            nameAndValue = pairs.get(4).split(":");
            father = new Person();
            mother = new Person();
            if(nameAndValue[0].equals("father\"")){
                father.savePairs(pairs.get(4));
                mother.savePairs(pairs.get(5));
            } else {
                mother.savePairs(pairs.get(4));
                father.savePairs(pairs.get(5));
            }
        }
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        String json = in.readUTF();

        savePairs(json);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {

        out.writeUTF("{\"root\":" + this.toStringJSON() + "}");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }
}
