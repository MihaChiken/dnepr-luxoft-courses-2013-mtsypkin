package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.*;

public class RedisDaoImpl<E extends Redis> extends AbstractDao {
    public E save(E entity) throws UserAlreadyExist {
        return (E) super.save(entity);
    }
    public E update(E entity) throws UserNotFound {
        return (E) super.update(entity);
    }

    public E get(long id) {
        return (E) super.get(id);
    }
}

