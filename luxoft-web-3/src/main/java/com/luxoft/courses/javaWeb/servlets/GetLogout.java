package com.luxoft.courses.javaWeb.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;


public class GetLogout extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Enumeration<String> sessionAttributeNames = session.getAttributeNames();
        String login = "";

        while (sessionAttributeNames.hasMoreElements()) {
            if (sessionAttributeNames.nextElement().equals("login")) {
                login = session.getAttribute("login").toString();
            }
        }
        if (!login.isEmpty()) {
            session.removeAttribute("login");
        }
        response.sendRedirect("/index.html");

    }
}
