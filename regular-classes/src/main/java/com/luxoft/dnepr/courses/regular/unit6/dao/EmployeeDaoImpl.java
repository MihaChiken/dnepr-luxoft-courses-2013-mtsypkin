package com.luxoft.dnepr.courses.regular.unit6.dao;

import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Employee;

public class EmployeeDaoImpl<E extends Employee> extends AbstractDao {

    public  E save(E entity) throws EntityAlreadyExistException {
        return (E) super.save(entity);
    }
    public E update(E entity) throws EntityNotFoundException {
        return (E) super.update(entity);
    }

    public E get(long id) {
        return (E) super.get(id);
    }

}


