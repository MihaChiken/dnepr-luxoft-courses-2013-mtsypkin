package com.luxoft.courses.javaWeb;


public interface Constants {
    String ACTIVE_SESSION_ATTRIBUTE = "ACTIVE_SESSIONS";
    String USERS_STORAGE_ATTRIBUTE = "USERS_STORAGE";

    String USER_LOGIN_ATTRIBUTE = "login";


    String MAIN_PAGE_URL = "/index.html";
    String USER_PAGE_URL = "/user";
}
