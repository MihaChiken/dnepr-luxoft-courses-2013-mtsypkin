package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct implements Product {
    double weight;

    Bread(String inpCode, String inpName, double inpPrice, double inpWeight) {
        setName(inpName);
        setCode(inpCode);
        setPrice(inpPrice);
        weight = inpWeight;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double inputWeight) {
        weight = inputWeight;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode() + Double.valueOf(weight).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass())
            return false;

        return super.equals(obj) && ((Bread) obj).weight == this.weight;
    }
    @Override
    public Object clone() throws CloneNotSupportedException {
        Bread obj = (Bread) super.clone();
        obj.setWeight(weight);
        return obj;
    }
}
