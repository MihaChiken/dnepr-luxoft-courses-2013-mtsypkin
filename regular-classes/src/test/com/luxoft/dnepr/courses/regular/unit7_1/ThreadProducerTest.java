package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ThreadProducerTest {
    @Test
    public void testGetNewThread() throws Exception {
        //ssertEquals(new Thread(),ThreadProducer.getNewThread());
    }

    @Test
    public void testGetRunnableThread() throws Exception {
        assertEquals(Thread.State.RUNNABLE,ThreadProducer.getRunnableThread().getState());
    }

    @Test
    public void testGetBlockedThread() throws Exception {
        assertEquals(Thread.State.BLOCKED,ThreadProducer.getBlockedThread().getState());
    }

    @Test
    public void testGetWaitingThread() throws Exception {
        assertEquals(Thread.State.WAITING,ThreadProducer.getWaitingThread().getState());
    }

    @Test
    public void testGetTimedWaitingThread() throws Exception {
        assertEquals(Thread.State.TIMED_WAITING,ThreadProducer.getTimedWaitingThread().getState());
    }

    @Test
    public void testGetTerminatedThread() throws Exception {
        assertEquals(Thread.State.TERMINATED,ThreadProducer.getTerminatedThread().getState());
    }
}
