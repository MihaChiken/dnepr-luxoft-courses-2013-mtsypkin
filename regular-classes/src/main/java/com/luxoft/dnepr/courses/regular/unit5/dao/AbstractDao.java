package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

public abstract class AbstractDao implements IDao {
    private Long getMaxId(){
        long maxId = 0;
        for(Long id: EntityStorage.getEntities().keySet()){
            if(id > maxId)
                maxId = id;
        }
        return maxId;
    }
    private boolean containsKey(Long key){
        for(Long id: EntityStorage.getEntities().keySet()){
            if(id.equals(key))
                return true;
        }
        return false;
    }
    @Override
    public Entity save(Entity entity) throws UserAlreadyExist {
        if(entity.getId() == null){
            entity.setId(getMaxId()+1);
        } else {
            if(containsKey(entity.getId())) {
                throw new UserAlreadyExist("User with the same id(" + entity.getId() + ") is already exist");
            }
        }
        EntityStorage.getEntities().put(entity.getId(),entity);
        return entity;
    }

    @Override
    public Entity update(Entity entity) throws UserNotFound {
        if(entity.getId() == null || !containsKey(entity.getId())){
            throw new UserNotFound("Thare is no user with id:"+entity.getId());
        }
        EntityStorage.getEntities().put(entity.getId(),entity);
        return entity;
    }

    @Override
    public Entity get(long id) {
        if(!containsKey(id)){
            return null;
        }
        return EntityStorage.getEntities().get(id);
    }

    @Override
    public boolean delete(long id) {
        if(!containsKey(id)) {
            return false;
        }
        EntityStorage.getEntities().remove(id);
        return true;
    }
}
