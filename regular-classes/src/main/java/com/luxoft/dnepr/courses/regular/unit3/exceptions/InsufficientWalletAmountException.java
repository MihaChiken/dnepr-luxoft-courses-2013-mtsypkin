package com.luxoft.dnepr.courses.regular.unit3.exceptions;


import java.math.BigDecimal;

public class InsufficientWalletAmountException extends Exception {
    private Long walletId;
    private BigDecimal amountToWithdraw;
    private BigDecimal amountInWallet;
    public InsufficientWalletAmountException(Long inpWalletId, BigDecimal inpAmountToWithdraw, BigDecimal inpAmountInWallet, String message){
        super(message);
        walletId = inpWalletId;
        amountToWithdraw = inpAmountToWithdraw;
        amountInWallet = inpAmountInWallet;
    }

    public Long getWalletId(){
        return walletId;
    }

    public  BigDecimal getAmountToWithdraw(){
        return amountToWithdraw;
    }

    public BigDecimal getAmountInWallet(){
        return amountInWallet;
    }
}
