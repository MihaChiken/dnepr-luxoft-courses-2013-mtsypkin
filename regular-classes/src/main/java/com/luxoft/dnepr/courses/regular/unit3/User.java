package com.luxoft.dnepr.courses.regular.unit3;

import java.math.BigDecimal;

public class User implements UserInterface {
    private Long id;
    private String name;
    private WalletInterface wallet;
    User(){}
    User(int id,String name, double amount, double maxAmount){
        this.id = (long)id;
        this.name = name;
        wallet = new Wallet();
        wallet.setAmount(BigDecimal.valueOf(amount));
        wallet.setMaxAmount(BigDecimal.valueOf(maxAmount));
        wallet.setStatus(WalletStatus.ACTIVE);
    }
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public WalletInterface getWallet() {
        return wallet;
    }

    @Override
    public void setWallet(WalletInterface wallet) {
        this.wallet = wallet;
    }
}
