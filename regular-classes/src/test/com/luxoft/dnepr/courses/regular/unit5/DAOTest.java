package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.*;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.*;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DAOTest {
    IDao<Redis> redisDao;
    IDao<Employee> employeeDao;
    Employee employee;
    Redis redis;
    @Before
    public void beforeTesting(){
        redisDao = new RedisDaoImpl();
        employeeDao = new EmployeeDaoImpl();

        employee = new Employee();
        redis = new Redis();
        redis.setId((long) 2);
        redis.setWeight(200);
        employee.setId((long) 3);
        employee.setSalary(4000);
    }
    @After
    public void afterTesting(){
        EntityStorage.getEntities().clear();
    }
    @Test
    public void saveTest() throws Exception{

        redis = redisDao.save(redis);
        employee = employeeDao.save(employee);

        Assert.assertEquals(EntityStorage.getEntities().size(), 2);
        Assert.assertEquals(EntityStorage.getEntities().get(redis.getId()).getClass().getSimpleName(), "Redis");
        Assert.assertEquals(EntityStorage.getEntities().get(employee.getId()).getClass().getSimpleName(), "Employee");

        redis.setId(null);
        redis = redisDao.save(redis);
        Assert.assertEquals(EntityStorage.getEntities().size(), 3);
        Assert.assertEquals(EntityStorage.getEntities().get(redis.getId()).getClass().getSimpleName(), "Redis");
    }
    @Test
    public void updateTest() throws Exception{

        EntityStorage.getEntities().put(redis.getId(),redis);
        EntityStorage.getEntities().put(employee.getId(),employee);

        redis.setWeight(100);
        redisDao.update(redis);
        employee.setSalary(8000);
        employeeDao.update(employee);

        Assert.assertEquals(((Redis)EntityStorage.getEntities().get(redis.getId())).getWeight(),100);
        Assert.assertEquals(((Employee)EntityStorage.getEntities().get(employee.getId())).getSalary(),8000);
    }
    @Test
    public void getTest(){
        EntityStorage.getEntities().put(redis.getId(),redis);
        EntityStorage.getEntities().put(employee.getId(),employee);

        Assert.assertEquals(redisDao.get(1),null);
        Assert.assertEquals(redisDao.get(2),redis);
        Assert.assertEquals(employeeDao.get(3),employee);
    }
    @Test
    public void deleteTest(){
        EntityStorage.getEntities().put(redis.getId(),redis);
        EntityStorage.getEntities().put(employee.getId(),employee);

        Assert.assertFalse(redisDao.delete(1));
        Assert.assertEquals(EntityStorage.getEntities().size(),2);
        Assert.assertTrue(redisDao.delete(2));
        Assert.assertEquals(EntityStorage.getEntities().size(),1);
        Assert.assertTrue(employeeDao.delete(3));
        Assert.assertEquals(EntityStorage.getEntities().size(),0);
    }
    @Test(expected = UserAlreadyExist.class)
    public void testUserAlreadyExist() throws UserAlreadyExist {
        redis = redisDao.save(redis);
        redis = redisDao.save(redis);
    }
    @Test(expected = UserNotFound.class)
    public void testUserNotFound() throws UserNotFound {
        redis = redisDao.update(redis);

    }
}
