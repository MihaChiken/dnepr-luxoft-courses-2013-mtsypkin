package com.luxoft.dnepr.courses.toprank;

import junit.framework.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TopRankExecutorTestMy {
    @Test
    public void executeTest(){
        TopRankExecutor e = new TopRankExecutor(0.8,2);
        Map<String,String> pages = new HashMap<>();
        pages.put("http://no_address/A.html","<html>\n" +
                "<body>\n" +
                "<h1>Page A</h1>\n" +
                "<a href=\"http://no_address/B.html\">B</a>\n" +
                "<a href=\"http://no_address/C.html\">C</a>\n" +
                "</body>\n" +
                "</html>");
        pages.put("http://no_address/B.html","<html>\n" +
                "<body>\n" +
                "<h1>Page B</h1>\n" +
                "<a href=\"http://no_address/C.html\">C</a>\n" +
                "</body>\n" +
                "</html>");
        pages.put("http://no_address/C.html","<html>\n" +
                "<body>\n" +
                "<h1>Page C</h1>\n" +
                "</body>\n" +
                "</html>");
        TopRankResults results = e.execute(pages);
        assertEquals(results.getGraph().get("http://no_address/A.html").size(),0);
        assertEquals(results.getGraph().get("http://no_address/B.html").size(),1);
        assertEquals(results.getGraph().get("http://no_address/C.html").size(),2);

        assertEquals(results.getIndex().get("<html>").size(),3);
        assertEquals(results.getIndex().get("C</h1>").size(),1);


        assertEquals(results.getRanks().get("http://no_address/A.html").doubleValue() ,0.0666667, 0.000001);
        assertEquals(results.getRanks().get("http://no_address/B.html").doubleValue() ,0.09333338, 0.000001);
        assertEquals(results.getRanks().get("http://no_address/C.html").doubleValue() ,0.2533333, 0.000001);

    }
}
