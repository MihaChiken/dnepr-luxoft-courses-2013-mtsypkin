package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class NoUserFoundException extends Exception {
    private Long userId;
    public NoUserFoundException(Long inpUserId, String message){
        super(message);
        userId = inpUserId;
    }

    public  Long getUserId(){
        return userId;
    }




}
