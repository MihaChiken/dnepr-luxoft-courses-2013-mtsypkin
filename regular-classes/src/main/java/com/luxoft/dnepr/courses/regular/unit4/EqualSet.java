package com.luxoft.dnepr.courses.regular.unit4;

import java.util.*;

public class EqualSet implements Set {
    private List<Object> set;
    EqualSet(){
        set = new ArrayList<>();

    }
    @Override
    public int size() {
        return set.size();
    }

    @Override
    public boolean isEmpty() {
        return set.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return set.contains(o);
    }

    @Override
    public Iterator iterator() {
        return set.iterator();
    }

    @Override
    public Object[] toArray() {
        return set.toArray();
    }

    @Override
    public boolean add(Object o) {
        for(Object obj: set){
            if(obj != null && o != null){
                if (obj.equals(o)){
                    return false;
                }
            }else{
                if(obj == null && o == null){
                    return false;
                }
            }
        }
        return set.add(o);
    }

    @Override
    public boolean remove(Object o) {
        return set.remove(o);
    }

    @Override
    public boolean containsAll(Collection c) {
        return set.containsAll(c);
    }

    @Override
    public boolean addAll(Collection c) {
        for(Object obj : c){
            add(obj);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection c) {
        return set.retainAll(c);
    }

    @Override
    public boolean removeAll(Collection c) {
        return set.removeAll(c);
    }

    @Override
    public void clear() {
        set.clear();
    }

    @Override
    public Object[] toArray(Object[] a) {
        return set.toArray(a);
    }
}
