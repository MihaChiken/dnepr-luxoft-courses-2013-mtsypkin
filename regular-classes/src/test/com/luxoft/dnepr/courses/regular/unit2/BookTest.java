package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book)book.clone();

        assertTrue(book.equals(cloned));

        Date date = new GregorianCalendar(2006, 0, 1).getTime();
        cloned.setPublicationDate(date);

        assertFalse(book.equals(cloned));

        book.setPublicationDate(date);

        assertTrue(book.equals(cloned));

        book.setName("Hello world!");

        assertFalse(book.equals(cloned));

    }

    @Test
    public void testEquals() throws Exception {
        Date date = new GregorianCalendar(2006, 0, 1).getTime();
        Book book1 = productFactory.createBook("code", "Thinking in Java", 200, date);
        Book book2 = productFactory.createBook("code", "Thinking in Java", 22, date);
        Book book3 = productFactory.createBook("cod", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());

        assertTrue(book1.equals(book2));
        assertFalse(book2.equals(book3));

    }
}
