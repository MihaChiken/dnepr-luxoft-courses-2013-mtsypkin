package com.luxoft.courses.javaWeb;


import java.util.concurrent.atomic.AtomicInteger;

public class CurrentStatistics {
    private AtomicInteger userSessions;
    private AtomicInteger adminSessions;

    private AtomicInteger getRequests;
    private AtomicInteger postRequests;
    private AtomicInteger otherRequests;

    public CurrentStatistics() {
        this.userSessions = new AtomicInteger(0);
        this.adminSessions = new AtomicInteger(0);
        this.getRequests = new AtomicInteger(0);
        this.postRequests = new AtomicInteger(0);
        this.otherRequests = new AtomicInteger(0);
    }

    public AtomicInteger getUserSessions() {
        return userSessions;
    }

    public AtomicInteger getAdminSessions() {
        return adminSessions;
    }

    public AtomicInteger getGetRequests() {
        return getRequests;
    }

    public AtomicInteger getPostRequests() {
        return postRequests;
    }

    public AtomicInteger getOtherRequests() {
        return otherRequests;
    }
}
