package com.luxoft.dnepr.courses.regular.unit18;

import java.util.Scanner;


public class UI implements Runnable {
    private static final String USER_RESPONSE_HELP = "help";
    private static final String USER_RESPONSE_EXIT = "exit";
    private static final String USER_RESPONSE_YES = "y";

    public void run() {
        String word, userResponse;
        do {
            Vocabulary vocabulary = new Vocabulary();
            word = vocabulary.random();

            System.out.println("Do you know translation of this word?:");
            System.out.println(word);

            Scanner scanner = new Scanner(System.in);
            userResponse = scanner.next();
            checkUserResponse(userResponse, word, vocabulary.getVocabulary().size());
        } while (!userResponse.equals(USER_RESPONSE_EXIT));
    }

    public static void main(String[] args) {
        UI ui = new UI();
        ui.run();
    }

    private void checkUserResponse(String userResponse, String word, int vocabularySize) {
        if (userResponse.isEmpty()) {
            printError();
        } else {
            switch (userResponse.toLowerCase()) {
                case USER_RESPONSE_EXIT:
                    Statistics.result(vocabularySize);
                    break;
                case USER_RESPONSE_HELP:
                    printHelp();
                    break;
                case USER_RESPONSE_YES:
                    Statistics.isKnownWord(word, true);
                    break;
                default:
                    Statistics.isKnownWord(word, false);
            }
        }
    }

    private void printHelp() {
        System.out.println("Lingustic analizator v1, autor Tushar Brahmacobalol for help type help, isKnownWord y/n question, your english knowlege will give you");
    }
    private void printError() {
        System.out.println("Error while reading response");
    }
}
