package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class WalletTest {
    @Test
    public void testCheckWithdrawal()  {
        Wallet wallet = new Wallet();
        wallet.setStatus(WalletStatus.BLOCKED);
        wallet.setAmount(BigDecimal.valueOf(100));

        try{
            wallet.checkWithdrawal(BigDecimal.valueOf(50));
            Assert.fail("Withdrawal from blocked wallet must invoke WalletIsBlockedException");
        } catch (WalletIsBlockedException e) {
            assertEquals(e.getMessage(),"Wallet is blocked");
        } catch (InsufficientWalletAmountException e) {
            Assert.fail("Withdrawal amount from blocked wallet must invoke WalletIsBlockedException");
        }

        wallet.setStatus(WalletStatus.ACTIVE);

        try{
            wallet.checkWithdrawal(BigDecimal.valueOf(150.234));
            Assert.fail("Withdrawal amount that less than amount in wallet must invoke InsufficientWalletAmountException");
        } catch (WalletIsBlockedException e) {
            e.printStackTrace();
        } catch (InsufficientWalletAmountException e) {
            assertEquals(e.getMessage(), "Insufficient wallet amount (100.00 < 150.23)");
        }
    }

    @Test
    public void testWithdraw() {
        Wallet wallet;
        wallet = new Wallet();
        wallet.setAmount(BigDecimal.valueOf(20));

        wallet.withdraw(BigDecimal.valueOf(9.5));
        assertEquals(wallet.getAmount(), BigDecimal.valueOf(10.5));
        wallet.withdraw(BigDecimal.valueOf(9.5));
        assertEquals(wallet.getAmount(),BigDecimal.valueOf(1.0));
    }

    @Test
    public void testCheckTransfer(){
        Wallet wallet = new Wallet();
        wallet.setStatus(WalletStatus.BLOCKED);
        wallet.setAmount(BigDecimal.valueOf(100));
        wallet.setMaxAmount(BigDecimal.valueOf(102));

        try{
            wallet.checkTransfer(BigDecimal.valueOf(1));
            Assert.fail("Transfer from blocked wallet must invoke WalletIsBlockedException");
        } catch (WalletIsBlockedException e) {
            assertEquals(e.getMessage(),"Wallet is blocked");
        } catch (LimitExceededException e) {
            e.printStackTrace();
        }

        wallet.setStatus(WalletStatus.ACTIVE);

        try{
            wallet.checkTransfer(BigDecimal.valueOf(150.234));
            Assert.fail("Transfer amount that make amount bigger than limit must invoke LimitExceededException");
        } catch (WalletIsBlockedException e) {
            e.printStackTrace();
        }  catch (LimitExceededException e) {
            assertEquals(e.getMessage(), "Wallet limit exceeded (102.00 < 100.00 + 150.23)");
        }

    }

    @Test
    public void testTransfer() throws Exception {
        Wallet wallet;
        wallet = new Wallet();
        wallet.setAmount(BigDecimal.valueOf(20));

        wallet.transfer(BigDecimal.valueOf(9.5));
        assertEquals(wallet.getAmount(), BigDecimal.valueOf(29.5));
        wallet.transfer(BigDecimal.valueOf(0.5));
        assertEquals(wallet.getAmount(),BigDecimal.valueOf(30.0));

    }
}
