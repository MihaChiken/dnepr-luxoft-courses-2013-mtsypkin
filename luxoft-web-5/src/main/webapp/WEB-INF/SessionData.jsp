
<%@ page import="com.luxoft.courses.javaWeb.CurrentStatistics" %>
<%@ page import="com.luxoft.courses.javaWeb.Constants" %>
<jsp:include page="/WEB-INF/include/header.jsp"/>

<h2>Session data:</h2>
<table class="sessionsTable">
    <tr class="tableHeader">
        <td>Parameter</td>
        <td>Value</td>
    </tr>
    <tr>
        <td>Active Sessions</td>
        <td>${ACTIVE_SESSIONS.userSessions + ACTIVE_SESSIONS.adminSessions}</td>
    </tr>
    <tr>
        <td>Active Sessions(ROLE user)</td>
        <td>${ACTIVE_SESSIONS.userSessions}</td>
    </tr>
    <tr>
        <td>Active Sessions(ROLE admin)</td>
        <td>${ACTIVE_SESSIONS.adminSessions}</td>
    </tr>
    <tr>
        <td>Total Count of HttpRequest</td>
        <td>${ACTIVE_SESSIONS.getRequests+ACTIVE_SESSIONS.postRequests+ACTIVE_SESSIONS.otherRequests}</td>
    </tr>
    <tr>
        <td>Total Count of POST HttpRequest</td>
        <td>${ACTIVE_SESSIONS.postRequests}</td>
    </tr>
    <tr>
        <td>Total Count of GET HttpRequest</td>
        <td>${ACTIVE_SESSIONS.getRequests}</td>
    </tr>
    <tr>
        <td>Total Count of Other HttpRequest</td>
        <td>${ACTIVE_SESSIONS.otherRequests}</td>
    </tr>
</table>
<jsp:include page="/WEB-INF/include/footer.jsp"/>