SELECT
  m.maker_id,
  AVG(l.screen) AS avg_size
FROM makers AS m

  INNER JOIN product AS p
    ON m.maker_id = p.maker_id
  INNER JOIN laptop AS l
    ON p.model = l.model
WHERE p.model IS NOT NULL
GROUP BY m.maker_id
ORDER BY avg_size

