CREATE TABLE IF NOT EXISTS makers (
	maker_id INT AUTO_INCREMENT,
	maker_name VARCHAR(50) NOT NULL,
	maker_adress VARCHAR(200) DEFAULT NULL,
	CONSTRAINT pk_makers PRIMARY KEY(maker_id)
);

INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (1, 'A', 'AdressA');
INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (2, 'B', 'AdressB');
INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (3, 'C', 'AdressC');
INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (4, 'D', 'AdressD');
INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (5, 'E', 'AdressE');


ALTER TABLE product ADD COLUMN maker_id INT(10) NOT NULL;
UPDATE product SET maker_id=1 WHERE maker='A';
UPDATE product SET maker_id=2 WHERE maker='B';
UPDATE product SET maker_id=3 WHERE maker='C';
UPDATE product SET maker_id=4 WHERE maker='D';
UPDATE product SET maker_id=5 WHERE maker='E';
ALTER TABLE product DROP COLUMN maker;
ALTER TABLE product ADD CONSTRAINT fk_product_makers FOREIGN KEY(maker_id) REFERENCES makers(maker_id);


CREATE TABLE IF NOT EXISTS printer_type (
  type_id INT,
  type_name VARCHAR(50),
  CONSTRAINT pk_printer_type PRIMARY KEY(type_id)
);

INSERT INTO printer_type(type_id, type_name) VALUES (1, 'Laser');
INSERT INTO printer_type(type_id, type_name) VALUES (2, 'Jet');
INSERT INTO printer_type(type_id, type_name) VALUES (3, 'Matrix');


ALTER TABLE printer ADD COLUMN type_id INT(10) NOT NULL;
UPDATE printer SET type_id=1 WHERE type='Laser';
UPDATE printer SET type_id=2 WHERE type='Jet';
UPDATE printer SET type_id=3 WHERE type='Matrix';

ALTER TABLE printer DROP COLUMN type;
ALTER TABLE printer ADD CONSTRAINT fk_printer_type FOREIGN KEY(type_id) REFERENCES printer_type(type_id);


ALTER TABLE printer MODIFY COLUMN color CHAR(1) NOT NULL DEFAULT 'y';

ALTER TABLE  pc ADD INDEX ind_pc_price(price);
ALTER TABLE  laptop ADD INDEX ind_laptop_price(price);
ALTER TABLE  printer ADD INDEX ind_printer_price(price);