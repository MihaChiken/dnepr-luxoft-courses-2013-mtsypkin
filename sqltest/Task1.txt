﻿Установить и развернуть локальный MySQL-сервер (mysql-installer-community-5.6.10.1.msi)
(Дополнительно) Установить MySQL Workbench 5.2, может потребовать обновить Microsoft .NET Framework до 4-й версии
Развернуть тестовый проект sqltest

1. Выделить информацию по производитеям в отдельную таблицу-справочник:

 -  создать таблицу-справочник производителей makers. Спецификация полей:
	maker_id (INT, является первичным ключом)
	maker_name (VARCHAR, 50 символов, не допускает NULL-значений)
	maker_adress (VARCHAR, 200 символов, может быть пустым)

 - добавить в makers данные по производителям (названия производителей: 'A', 'B', 'C', 'D', 'E'), адреса ('AdressA', 'AdressB'...)

 - yдалить из product поле maker, доавить вместо него поле maker_id - внешний ключ к таблице makers (не уникальное, не допускает NULL).
 Соответствующий constraint назвать fk_product_makers

2. Вынести информацию по типам принтеров в отдельную таблицу-справочник:

 - создать таблицу-справочник типов принтеров printer_type. Спецификация полей:
 	type_id (INT, является первичным ключом (pk_printer_type)
 	type_name (VARCHAR, 50 символов, не допускает NULL)

  - добавить в таблицу данные по типам принтеров ('Matrix', 'Laser', 'Jet')

  - yдалить из printer поле type, добавить вместо него поле type_id - внешний ключ к таблице printer_type (не уникальное, не допускает NULL).
   Соответствующий constraint назвать fk_printer_type

3. Изменить поле printer.color так, чтобы оно не могло соДержать null и по умолчанию принимало бы значение 'y'

4. Создать индексы для таблиц с продукцией (pc, laptop, printer) по полю price. Называние индекса должно формироваться по схеме
ind_<имя_таблицы>_<имя поля>.

5. Поместить полученные скрипты в файл alter_schema.sql и разместить его вместе с остальными скриптами в resources/script/schema

6. Создать скрипт resources/script/schema/drop_schema.sql, в который поместить инструкции по удалению всех создаваемых в БД структур

