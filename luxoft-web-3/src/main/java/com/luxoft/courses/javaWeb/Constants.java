package com.luxoft.courses.javaWeb;


public interface Constants {
    String ACTIVE_SESSION_ATTRIBUTE = "ACTIVE_SESSIONS";
    String USERS_STORAGE_ATTRIBUTE = "USERS_STORAGE";
}
