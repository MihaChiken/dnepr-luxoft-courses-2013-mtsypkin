package com.luxoft.courses.javaWeb;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Dummy extends HttpServlet {
    private static Map<String, Integer> users = new ConcurrentHashMap<>();

    private void showError(HttpServletResponse response, String message) throws IOException {
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();

        out.println("{\"error\": \"" + message + "\"}");

    }

    private boolean checkParams(HttpServletRequest request) {
        Enumeration<String> parameterNames = request.getParameterNames();
        int i = 0;
        while (parameterNames.hasMoreElements()) {
            String param = parameterNames.nextElement();
            if (param.equals("name")) {
                i++;
            }
            if (param.equals("age")) {
                i++;
            }
        }
        return i == 2;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (!checkParams(request)) {
            showError(response, "Illegal parameters");
        } else {
            if (!users.containsKey(request.getParameter("name"))) {
                showError(response, "Name " + request.getParameter("name") + " does not exist");
            } else {
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
                users.put(request.getParameter("name"), Integer.parseInt(request.getParameter("age")));

            }
        }
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!checkParams(request)) {
            showError(response, "Illegal parameters");
        } else {
            if (users.containsKey(request.getParameter("name"))) {
                showError(response, "Name " + request.getParameter("name") + " already exist");
            } else {
                response.setStatus(HttpServletResponse.SC_CREATED);
                users.put(request.getParameter("name"), Integer.parseInt(request.getParameter("age")));

            }
        }
    }

}
