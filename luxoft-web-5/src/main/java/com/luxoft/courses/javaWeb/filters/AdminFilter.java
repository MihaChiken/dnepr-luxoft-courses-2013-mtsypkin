package com.luxoft.courses.javaWeb.filters;

import com.luxoft.courses.javaWeb.Constants;
import com.luxoft.courses.javaWeb.Users;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;

public class AdminFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        if (LoginFilter.checkLogin(request)) {
            Users users =(Users) request.getServletContext().getAttribute(Constants.USERS_STORAGE_ATTRIBUTE);
            String login = request.getSession().getAttribute(Constants.USER_LOGIN_ATTRIBUTE).toString();

            if (users.checkAdminAccessFor(login)) {
                chain.doFilter(req, resp);
            } else {
                response.sendRedirect(request.getContextPath()+Constants.USER_PAGE_URL);
            }

        } else {
            response.sendRedirect(request.getContextPath()+Constants.MAIN_PAGE_URL);
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
