package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Map;

public class Bank implements BankInterface {
    private Map<Long, UserInterface> users;

    Bank(String expectedJavaVersion) {
        if (!System.getProperty("java.version").equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(System.getProperty("java.version"), expectedJavaVersion, "Illegal Java version.");
        }
    }
    private String format(BigDecimal decimal){
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        DecimalFormatSymbols custom = new DecimalFormatSymbols();
        custom.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(custom);
        return df.format(decimal);
    }
    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }


    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {

        try {
            users.get(fromUserId).getWallet().checkWithdrawal(amount);
        } catch (NullPointerException e) {
            throw new NoUserFoundException(fromUserId, "Unknown user with id: " + fromUserId);
        } catch (WalletIsBlockedException  e) {
            throw new TransactionException("User '"+users.get(fromUserId).getName()+"' wallet is blocked");
        } catch (InsufficientWalletAmountException e){
            throw new TransactionException("User '"+users.get(fromUserId).getName()+"' has insufficient funds ("+format(users.get(fromUserId).getWallet().getAmount())+" < "+format(amount)+")");
        }

        try {
            users.get(toUserId).getWallet().checkTransfer(amount);
        } catch (NullPointerException e) {
            throw new NoUserFoundException(fromUserId, "Unknown user with id: " + toUserId);
        } catch (WalletIsBlockedException e) {
            throw new TransactionException("User '"+users.get(toUserId).getName()+"' wallet is blocked");
        } catch (LimitExceededException e){
            throw new TransactionException("User '"+users.get(toUserId).getName()+"' wallet limit exceeded ("+format(users.get(toUserId).getWallet().getAmount())+
                    " + "+format(amount)+" > "+format(users.get(toUserId).getWallet().getMaxAmount())+")");
        }

        users.get(fromUserId).getWallet().withdraw(amount);
        users.get(toUserId).getWallet().transfer(amount);
    }
}
