package com.luxoft.dnepr.courses.compiler;

import org.junit.Assert;
import org.junit.Test;


import static org.junit.Assert.*;

public class MyCompilerTest extends AbstractCompilerTest {

    @Test
    public void addAndSubTest() {
        assertCompiled(3.5, "5 - 2 + 3 - 2.5");
        assertCompiled(10.5, "2.1 + 2.9 + 3 + 2.5");
    }

    @Test
    public void associationTest() {
        assertCompiled(6.0, "2 + 2 * 2");
        assertCompiled(2.0, "2 + 2 * 2 - 4 / 2 * 2");
        assertCompiled(3.3, "20.3 - 3 * 6 + 1");
    }

    @Test
    public void bracketsTest() {
        assertCompiled(2.5, "(1 + 4) / (1.5 + 0.5)");
        assertCompiled(8.0, "(((2 + 2) * 3) / (2 + 1)) * (4 - 2)");
        assertCompiled(62.0, "2 * (1 + 2*(1 + 2*(1 + 2*(1+2))))");

    }

    @Test
    public void IllegalInputExceptionTest() {
        try {
            assertCompiled(2.5, "e - 1");
            Assert.fail("Compiling 'e - 1' must invoke CompilationException");
        } catch (CompilationException e) {
            assertEquals(e.getMessage(), "Wrong input format");
        }
        try {
            assertCompiled(62.0, "2 * (1 + 2*(1 + 2*(1 + 2*(1+2)))");
            Assert.fail("Compiling '2 * (1 + 2*(1 + 2*(1 + 2*(1+2)))' must invoke CompilationException");
        } catch (CompilationException e) {
            assertEquals(e.getMessage(), "Wrong input format");
        }
        try {
            assertCompiled(62.0, "2++2-3");
            Assert.fail("Compiling '2++2-3)' must invoke CompilationException");
        } catch (CompilationException e) {
            assertEquals(e.getMessage(), "Wrong input format");
        }
        try {
            assertCompiled(62.0, "+2-2 3");
            Assert.fail("Compiling '+2-2 3' must invoke CompilationException");
        } catch (CompilationException e) {
            assertEquals(e.getMessage(), "Wrong input format");
        }
    }
}
