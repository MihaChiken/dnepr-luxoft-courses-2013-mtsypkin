package com.luxoft.dnepr.courses.toprank;

import java.util.*;

public class TopRankExecutor {
    private double dampingFactor;
    private int numberOfLoopsInRankComputing;

    private List<String> linkGetter(String content) {
        List<String> linksList = new ArrayList<String>();
        StringBuilder linkToAdd;
        for (String link : content.split("href=")) {
            if (link.charAt(0) == '"') {
                linkToAdd = new StringBuilder("");
                for (int i = 1; link.charAt(i) != '"'; i++) {
                    linkToAdd.append(link.charAt(i));
                }
                linksList.add(linkToAdd.toString());
            }
        }
        return linksList;
    }

    private void addWordsToIndex(Map<String, List<String>> index, String[] words, String link) {
        for (String word : words) {
            if (index.containsKey(word)) {
                index.get(word).add(link);
            } else {
                List<String> newList = new ArrayList<>();
                newList.add(link);
                index.put(word, newList);
            }
        }

    }

    private void setGraph(TopRankResults results, int start, int stop) {
        List<String> graphLinks;
        Object[] keys = results.getReverseGraph().keySet().toArray();
        for (int i = start; i < stop; i++) {
            String key = (String) keys[i];
            graphLinks = new ArrayList<>();
            for (String link : results.getReverseGraph().keySet()) {
                for (String url : results.getReverseGraph().get(link)) {
                    if (url.equals(key)) {
                        graphLinks.add(link);
                    }
                }
            }
            results.getGraph().put(key, graphLinks);
        }

    }

    private void countRanks(TopRankResults results) {
        Map<String, Double> ranks = results.getRanks();
        Map<String, List<String>> graph = results.getGraph();
        Map<String, List<String>> reverseGraph = results.getReverseGraph();
        int numberOfPages = graph.size();
        for (String page : graph.keySet()) {
            ranks.put(page, (1.0 / numberOfPages));
        }
        double rank;
        Map<String, Double> tempRanks;
        for (int currentLoop = 0; currentLoop < numberOfLoopsInRankComputing; currentLoop++) {
            tempRanks = new HashMap<>(ranks);
            for (String page : tempRanks.keySet()) {
                rank = (1 - dampingFactor) / numberOfPages;
                for (String linkedPage : graph.get(page)) {
                    rank += dampingFactor * tempRanks.get(linkedPage) / reverseGraph.get(linkedPage).size();
                }
                ranks.put(page, rank);
            }
        }
    }

    public TopRankExecutor(double dampingFactor, int numberOfLoopsInRankComputing) {
        this.dampingFactor = dampingFactor;
        this.numberOfLoopsInRankComputing = numberOfLoopsInRankComputing;
    }
    private void countReverseGraphAndIndex(Map<String, String> urlContent,TopRankResults results){
        Thread thread1, thread2,thread3;
        try {
            thread1 = new Thread(new RunRGraphAndIndex(0, urlContent.size() / 3, urlContent, results));
            thread2 = new Thread(new RunRGraphAndIndex(urlContent.size() / 3, urlContent.size()* 2 / 3 , urlContent, results));
            thread3 = new Thread(new RunRGraphAndIndex(urlContent.size()* 2 / 3 , urlContent.size(), urlContent, results));
            thread1.start();
            thread2.start();
            thread3.start();

            thread1.join();
            thread2.join();
            thread3.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    private void countGraph(TopRankResults results){
        Thread thread1, thread2, thread3;
        try {
            thread1 = new Thread(new RunGraph(0, results.getReverseGraph().size() /3, results));
            thread2 = new Thread(new RunGraph(results.getReverseGraph().size() / 3, results.getReverseGraph().size()* 2 / 3 , results));
            thread3 = new Thread(new RunGraph(results.getReverseGraph().size()* 2 / 3 , results.getReverseGraph().size(), results));
            thread1.start();
            thread2.start();
            thread3.start();

            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public TopRankResults execute(Map<String, String> urlContent) {
        TopRankResults results = new TopRankResults();

        countReverseGraphAndIndex(urlContent,results);
        countGraph(results);
        countRanks(results);

        return results;
    }

    class RunRGraphAndIndex implements Runnable {
        private int stop, start;
        private Object[] array;
        private final Map<String, String> urlContent;
        private TopRankResults results;

        RunRGraphAndIndex(int start, int stop, Map<String, String> urlContent, TopRankResults results) {
            this.stop = stop;
            this.start = start;
            this.array = urlContent.keySet().toArray();
            this.urlContent = urlContent;
            this.results = results;
        }

        @Override
        public void run() {
            for (int i = start; i < stop; i++) {
                String key = (String) array[i];
                results.getReverseGraph().put(key, linkGetter(urlContent.get(key)));
                addWordsToIndex(results.getIndex(), urlContent.get(key).split("[\t\n\\s]"), key);
            }
        }

    }

    class RunGraph implements Runnable {
        private int stop, start;
        private TopRankResults results;

        RunGraph(int start, int stop, TopRankResults results) {
            this.stop = stop;
            this.start = start;
            this.results = results;
        }

        @Override
        public void run() {
            setGraph(results, start, stop);
        }

    }
}
