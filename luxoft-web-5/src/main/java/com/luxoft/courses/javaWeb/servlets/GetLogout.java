package com.luxoft.courses.javaWeb.servlets;

import com.luxoft.courses.javaWeb.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;


public class GetLogout extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Enumeration<String> sessionAttributeNames = session.getAttributeNames();
        String login = "";

        while (sessionAttributeNames.hasMoreElements()) {
            if (sessionAttributeNames.nextElement().equals(Constants.USER_LOGIN_ATTRIBUTE)) {
                login = session.getAttribute(Constants.USER_LOGIN_ATTRIBUTE).toString();
            }
        }
        if (!login.isEmpty()) {
            session.invalidate();
        }
        response.sendRedirect(request.getContextPath()+Constants.MAIN_PAGE_URL);

    }
}
