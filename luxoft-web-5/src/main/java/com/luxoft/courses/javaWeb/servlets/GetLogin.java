package com.luxoft.courses.javaWeb.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import com.luxoft.courses.javaWeb.Users;
import com.luxoft.courses.javaWeb.Constants;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;


public class GetLogin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Users users =(Users) getServletContext().getAttribute(Constants.USERS_STORAGE_ATTRIBUTE);
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        if (users.isValidPair(login, password)) {
            request.getSession().setAttribute(Constants.USER_LOGIN_ATTRIBUTE, login);

            response.sendRedirect(request.getContextPath()+Constants.USER_PAGE_URL);
        } else {
            response.sendRedirect(request.getContextPath()+Constants.MAIN_PAGE_URL+"?invalid=1");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
