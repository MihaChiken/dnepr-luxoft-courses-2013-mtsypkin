package com.luxoft.dnepr.courses.regular.unit7_1;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public final class ThreadProducer {

    private ThreadProducer() {
    }

    public static Thread getNewThread() {
        return new Thread();
    }

    public static Thread getRunnableThread() throws InterruptedException {
        Thread t = new Thread();
        t.start();

        Thread.sleep(100);
        return t;
    }

    public static synchronized void makeBlocked() {
        int i = 1;
        while (true) {
            i++;
        }
    }

    public static Thread getBlockedThread() throws InterruptedException {
        Thread thread = new Thread() {
            @Override
            public void run() {
                makeBlocked();
            }
        };
        thread.start();

        Thread.sleep(100);
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                makeBlocked();
            }
        };
        thread1.start();

        Thread.sleep(100);
        return thread1;
    }

    public static Thread getWaitingThread() throws InterruptedException {
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread1.start();

        Thread.sleep(100);
        return thread1;
    }

    public static Thread getTimedWaitingThread() throws InterruptedException {
        Thread thread1 = new Thread() {

            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread1.start();

        Thread.sleep(100);
        return thread1;
    }

    public static Thread getTerminatedThread() throws InterruptedException {
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();

        TimeUnit.MILLISECONDS.sleep(1000);
        return t;
    }
}
