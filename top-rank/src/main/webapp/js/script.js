
$(document).ready(function(){
    $("#searchBar").focusin(function(){
        $("#tip").addClass("hidden");
    });

    $("#searchBar").focusout(function(){
            if($("#searchBar").val()===""){
                $("#tip").removeClass("hidden");
            }
    });
    $("#searchButton").click(function(){
        var dummyData = [
        { href : 'jquery.com', description : 'JQuery: very popular lightweight javascript library' },
        { href : 'w3.org', description : 'World wide web consorcium main web site' },
        { href : 'mozilla.org', description : 'Mozilla foundation resources' },
        { href : 'ru.wikipedia.org', description : 'Russian division of wikipedia - free internet enciclopedia' }
        ];
        var searchResults = "";
        for(var i in dummyData){
            searchResults +="<div class='searchResult'>"+
                             "<a href="+dummyData[i].href+">"+dummyData[i].href+"</a>"+
                             "<p>"+dummyData[i].description+"</p>"+
                         "</div>"
        }
        $("#searchResults").html(searchResults);
    });
});