package com.luxoft.courses.javaWeb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;


public class UserPage extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!checkLogin(request)) {
            response.sendRedirect("/index.html");
        } else {
            HttpSession session = request.getSession();
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Hello "+session.getAttribute("login")+"</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Hello "+session.getAttribute("login")+"</h1>");
            out.println("<p><a href='/logout'>logout</a></p>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    private boolean checkLogin(HttpServletRequest request){
        HttpSession session = request.getSession();
        Enumeration<String> sessionAttributeNames = session.getAttributeNames();
        String login = "";

        while (sessionAttributeNames.hasMoreElements()) {
            if (sessionAttributeNames.nextElement().equals("login")) {
                login = session.getAttribute("login").toString();
            }
        }
        return !login.isEmpty();
    }
}
