SELECT DISTINCT
  makers.maker_name
FROM makers, product, pc
WHERE product.maker_id = makers.maker_id AND
      pc.model = product.model AND
      pc.speed > 450
