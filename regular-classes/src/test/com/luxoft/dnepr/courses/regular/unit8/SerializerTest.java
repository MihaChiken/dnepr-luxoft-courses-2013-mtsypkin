package com.luxoft.dnepr.courses.regular.unit8;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class SerializerTest {
    @Test
    public void complexTest() throws Exception {
        File file = new File("test1.txt");

        Person person = new Person();
        person.setBirthDate(new Date());
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        person.setName("Galja");
        Person mother = new Person();
        mother.setBirthDate(new Date());
        mother.setGender(Gender.FEMALE);
        mother.setName("Ann");

        person.setMother(mother);
        Person father = new Person();
        father.setBirthDate(new Date());
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Vasiliy");
        mother.setFather(father);
        person.setFather(father);
        FamilyTree familyTree = new FamilyTree(person);

        Serializer.serialize(file, familyTree);

        FamilyTree familyTreeNew;
        familyTreeNew = Serializer.deserialize(new File("test1.txt"));
        familyTreeNew.getRoot();

        assertEquals(familyTree.getRoot().getName(),familyTreeNew.getRoot().getName());
        assertEquals(familyTree.getRoot().getEthnicity(),familyTreeNew.getRoot().getEthnicity());
        assertEquals(familyTree.getRoot().getBirthDate(),familyTreeNew.getRoot().getBirthDate());
        assertEquals(familyTree.getRoot().getMother().getFather().getName(),familyTreeNew.getRoot().getMother().getFather().getName());
    }
}
