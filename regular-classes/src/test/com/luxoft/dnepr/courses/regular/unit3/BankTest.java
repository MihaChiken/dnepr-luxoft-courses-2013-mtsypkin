package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BankTest {
    @Test
    public void testJavaVersionControl() {
        try {
            Bank bank = new Bank("1.6");
            Assert.fail("1.6 version must invoke IllegalJavaVersionError");
        } catch (IllegalJavaVersionError e) {
            assertEquals(e.getMessage(), "Illegal Java version.");
        }

    }

    @Test
    public void testMakeMoneyTransaction() throws TransactionException, NoUserFoundException {
        Map<Long, UserInterface> users = new HashMap<>();
        users.put((long) 1, new User(1, "Vasya", 20, 30));
        users.put((long) 2, new User(2, "Petya", 100, 300));

        Bank bank = new Bank("1.7.0_09");
        bank.setUsers(users);

        bank.makeMoneyTransaction((long) 1, (long) 2, BigDecimal.valueOf(10));
        users = bank.getUsers();
        assertTrue(users.get((long) 2).getWallet().getAmount().compareTo(BigDecimal.valueOf(110)) == 0);

        bank.makeMoneyTransaction((long) 2, (long) 1, BigDecimal.valueOf(15));
        users = bank.getUsers();
        assertTrue(users.get((long) 1).getWallet().getAmount().compareTo(BigDecimal.valueOf(25)) == 0);
    }

    @Test
    public void testIllegalExceptionsOfMoneyTransaction() {
        Map<Long, UserInterface> users = new HashMap<>();
        users.put((long) 1, new User(1, "Vasya", 20, 30));
        users.put((long) 2, new User(2, "Petya", 100, 300));
        users.put((long) 3, new User(3, "Kolya", 30, 200));

        Bank bank = new Bank("1.7.0_09");
        bank.setUsers(users);

        try {
            bank.makeMoneyTransaction((long) 4, (long) 2, BigDecimal.valueOf(10));
            Assert.fail("Transaction from unknown id=4 must invoke NoUserFoundException");
        } catch (NoUserFoundException e) {
            assertEquals(e.getMessage(), "Unknown user with id: 4");
        } catch (TransactionException e) {
            Assert.fail("Transaction from unknown id=4 must invoke NoUserFoundException");
        }

        try {
            bank.makeMoneyTransaction((long) 3, (long) 1, BigDecimal.valueOf(40));
            Assert.fail("Transaction more amount that there are in wallet must invoke TransactionException");
        } catch (NoUserFoundException e) {
            Assert.fail("Transaction more amount that there are in wallet must invoke TransactionException");
        } catch (TransactionException e) {
            assertEquals(e.getMessage(), "User 'Kolya' has insufficient funds (30.00 < 40.00)");
        }

        try {
            bank.makeMoneyTransaction((long) 2, (long) 1, BigDecimal.valueOf(40));
            Assert.fail("Transaction of amount that exceed wallet limit must invoke TransactionException");
        } catch (NoUserFoundException e) {
            Assert.fail("Transaction of amount that exceed wallet limit must invoke TransactionException");
        } catch (TransactionException e) {
            assertEquals(e.getMessage(), "User 'Vasya' wallet limit exceeded (20.00 + 40.00 > 30.00)");
        }
        bank.getUsers().get((long) 1).getWallet().setStatus(WalletStatus.BLOCKED);
        try {
            bank.makeMoneyTransaction((long) 2, (long) 1, BigDecimal.valueOf(5));
            Assert.fail("Transaction to the blocked wallet must invoke TransactionException");
        } catch (NoUserFoundException e) {
            Assert.fail("Transaction to the blocked wallet must invoke TransactionException");
        } catch (TransactionException e) {
            assertEquals(e.getMessage(), "User 'Vasya' wallet is blocked");
        }

    }
}
