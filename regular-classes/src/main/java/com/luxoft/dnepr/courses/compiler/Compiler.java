package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.*;

class Sign {
    Sign(char inputSign, int inputNest, int inputId) {
        val = Character.toString(inputSign);
        id = inputId;
        if (val.equals("+") || val.equals("-")) {
            if (val.equals("+"))
                nest = inputNest - 1;
            else
                nest = inputNest - 0.5;
        } else {
            nest = inputNest;
        }
    }

    String val;
    double nest;
    int id;
}

class SignsComparator implements Comparator<Sign> {
    @Override
    public int compare(Sign x, Sign y) {
        if (x.nest < y.nest) {
            return 1;
        }
        if (x.nest > y.nest) {
            return -1;
        }
        return 0;
    }
}

public class Compiler {

    public static final boolean DEBUG = true;

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static boolean validate(String input) {
        if (!input.matches("[\\d\\.\\d | \\d | \\s | \\( | \\) | \\+ | \\- | \\* | /]*")) {
            return false;
        }
        int open = 0, close = 0;
        for (char symbol : input.toCharArray()) {
            if (symbol == '(') {
                open++;
            }
            if (symbol == ')') {
                close++;
            }
        }
        return open == close;
    }

    static Sign[] getSigns(String input) {
        Comparator<Sign> comparator = new SignsComparator();
        List<Sign> signs =
                new ArrayList<Sign>();
        int nest = 1, i = 0;
        for (char symbol : input.toCharArray()) {
            if (symbol == '(') {
                nest += 2;
            }
            if (symbol == ')') {
                nest -= 2;
            }
            if (symbol == '+' ||
                    symbol == '-' ||
                    symbol == '*' ||
                    symbol == '/') {
                signs.add(new Sign(symbol, nest, i++));
            }
        }
        Collections.sort(signs, comparator);
        Sign signsArray[] = new Sign[signs.size()];
        int j = 0;
        for (Sign sign : signs) {
            signsArray[j++] = sign;
        }
        return signsArray;
    }

    static List<Double> getValues(String input) {
        String[] numbersSplit = input.split("\\+|\\-|\\*|/|\\(|\\)");
        List<Double> values = new ArrayList<Double>();
        for (String value : numbersSplit) {
            if (!value.trim().equals("")) {
                try {
                    values.add(Double.parseDouble(value));
                } catch (NumberFormatException e) {
                    throw new CompilationException("Wrong input format");
                }
            }
        }
        return values;
    }

    static byte[] compile(String input) {
        if (!validate(input)) {
            throw new CompilationException("Wrong input format");
        }
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        List<Double> values = getValues(input);
        Sign[] signs = getSigns(input);
        if (signs.length + 1 != values.size()) {
            throw new CompilationException("Wrong input format");
        }
        boolean swap;
        for (Sign sign : signs) {
            swap = true;
            //Pushing values
            if (values.get(sign.id) != null) {
                addCommand(result, VirtualMachine.PUSH, values.get(sign.id));
                values.set(sign.id, null);
                swap = false;
            }
            if (values.get(sign.id + 1) != null) {
                addCommand(result, VirtualMachine.PUSH, values.get(sign.id + 1));
                values.set(sign.id + 1, null);
                swap = !swap;
            }

            // Adding commands
            if (sign.val.equals("+")) {
                addCommand(result, VirtualMachine.ADD);
            }
            if (sign.val.equals("-")) {
                if (swap)
                    addCommand(result, VirtualMachine.SWAP);
                addCommand(result, VirtualMachine.SUB);
            }
            if (sign.val.equals("*")) {
                addCommand(result, VirtualMachine.MUL);
            }
            if (sign.val.equals("/")) {
                if (swap)
                    addCommand(result, VirtualMachine.SWAP);
                addCommand(result, VirtualMachine.DIV);
            }
        }
        addCommand(result, VirtualMachine.PRINT);
        return result.toByteArray();
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
