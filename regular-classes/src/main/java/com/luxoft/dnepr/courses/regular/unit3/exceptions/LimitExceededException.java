package com.luxoft.dnepr.courses.regular.unit3.exceptions;


import java.math.BigDecimal;

public class LimitExceededException extends Exception {
    private Long walletId;
    private BigDecimal amountToTransfer;
    private BigDecimal amountInWallet;

    public LimitExceededException(Long inpWalletId, BigDecimal inpAmountToTransfer, BigDecimal inpAmountInWallet, String message){
        super(message);
        walletId = inpWalletId;
        amountInWallet = inpAmountInWallet;
        amountToTransfer = inpAmountToTransfer;
    }

    public Long getWalletId(){
        return walletId;
    }

    public BigDecimal getAmountToTransfer(){
        return amountToTransfer;
    }
    public BigDecimal getAmountInWallet(){
        return amountInWallet;
    }

}
