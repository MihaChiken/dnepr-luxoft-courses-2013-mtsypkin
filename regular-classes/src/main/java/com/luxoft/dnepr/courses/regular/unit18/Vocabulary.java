package com.luxoft.dnepr.courses.regular.unit18;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class Vocabulary {
    public static final String sonnetsFilePath = "regular-classes/src/main/java/com/luxoft/dnepr/courses/regular/unit18/sonnets.txt";
    private Set<String> vocabulary = new HashSet<>();

    public Set<String> getVocabulary() {
        return vocabulary;
    }

    public Vocabulary() {
        File sonnets = new File(sonnetsFilePath);
        String words;
        try {
            words = getFileContent(sonnets);
            fillVocabulary(words,vocabulary);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String random() {
        return vocabulary.toArray(new String[]{})[(int) (Math.random() * vocabulary.size())];
    }

    private String getFileContent(File file) throws IOException {
        Reader reader = new InputStreamReader(new FileInputStream(file));
        StringBuilder words = new StringBuilder("");

        int readChar = reader.read();
        while (readChar > 0) {
            words.append((char) readChar);
            readChar = reader.read();
        }
        reader.close();

        return words.toString();
    }

    private void fillVocabulary(String words, Set<String> vocabulary) {
        StringTokenizer tokenizer = new StringTokenizer(words.toString());
        while (tokenizer.hasMoreTokens()) {
            String word = tokenizer.nextToken();
            if (word.length() > 3) vocabulary.add(word.toLowerCase());
        }
    }
}
