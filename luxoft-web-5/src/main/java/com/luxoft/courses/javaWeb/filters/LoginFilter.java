package com.luxoft.courses.javaWeb.filters;

import com.luxoft.courses.javaWeb.Constants;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;


public class LoginFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        if (checkLogin((HttpServletRequest) req)) {
            chain.doFilter(req, resp);
        } else {
            ((HttpServletResponse) resp).sendRedirect(((HttpServletRequest) req).getContextPath()+Constants.MAIN_PAGE_URL);
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

    public static boolean checkLogin(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Enumeration<String> sessionAttributeNames = session.getAttributeNames();
        String login = "";

        while (sessionAttributeNames.hasMoreElements()) {
            if (sessionAttributeNames.nextElement().equals(Constants.USER_LOGIN_ATTRIBUTE)) {
                login = session.getAttribute(Constants.USER_LOGIN_ATTRIBUTE).toString();
            }
        }
        return !login.isEmpty();
    }

}
