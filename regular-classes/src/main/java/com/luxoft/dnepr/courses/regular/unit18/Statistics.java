package com.luxoft.dnepr.courses.regular.unit18;

import java.util.ArrayList;
import java.util.List;

public class Statistics {
    public static List<String> known = new ArrayList<String>();
    public static List<String> unknown = new ArrayList<String>();

    public static void result(int totalWordCount) {
        int w = totalWordCount * (known.size() + 1) / (known.size() + unknown.size() + 1);
        System.out.println("Your estimated vocabulary is " + w + " words");
    }

    public static void isKnownWord(String word, Boolean isKnown) {
        if (isKnown) {
            known.add(word);
        } else {
            unknown.add(word);
        }
    }

}
