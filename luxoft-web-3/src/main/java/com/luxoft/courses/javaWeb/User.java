package com.luxoft.courses.javaWeb;

public class User {
    private String login;
    private String password;
    private String role;

    User(String name, String password, String role) {
        this.login = name;
        this.password = password;
        this.role = role;
    }

    String getLogin() {
        return login;
    }

    String getPassword() {
        return password;
    }

    String getRole() {
        return role;
    }

    public boolean isValidPair(String login, String password) {
        return  (this.login.equals(login) && this.password.equals(password));
    }
}
