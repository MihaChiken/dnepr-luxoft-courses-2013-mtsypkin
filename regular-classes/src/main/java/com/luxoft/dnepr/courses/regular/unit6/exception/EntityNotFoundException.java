package com.luxoft.dnepr.courses.regular.unit6.exception;

public class EntityNotFoundException extends Exception {
    public EntityNotFoundException(String message){
        super(message);
    }
}
