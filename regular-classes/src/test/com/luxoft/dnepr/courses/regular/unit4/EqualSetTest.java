package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;

public class EqualSetTest {

    TestEquals first, equalToFirst, second;

    @Before
    public void setValues() {
        first = new TestEquals(1);
        equalToFirst = new TestEquals(1);
        second = new TestEquals(3);
    }

    @Test
    public void testSize() throws Exception {
        Set set = new EqualSet();
        set.add(first);
        set.add(equalToFirst);
        set.add(second);

        assertEquals(2, set.size());
        set.add(first);
        assertEquals(2, set.size());
    }


    @Test
    public void testIsEmpty() throws Exception {
        Set set = new EqualSet();
        assertTrue(set.isEmpty());
        set.add(first);
        assertFalse(set.isEmpty());
        set.remove(first);
        assertTrue(set.isEmpty());
    }

    @Test
    public void testContains() throws Exception {
        Set set = new EqualSet();
        set.add(first);
        assertTrue(set.contains(first));
    }

    @Test
    public void testIterator() throws Exception {
        Set set = new EqualSet();
        set.add(first);
        set.add(second);
        Iterator i=set.iterator();
        assertTrue(i.hasNext());
        assertEquals(first,i.next());
        assertEquals(second,i.next());
        assertFalse(i.hasNext());

    }

    @Test
    public void testToArray() throws Exception {
        Set set = new EqualSet();
        set.add(first);
        set.add(second);

        TestEquals array[] = new TestEquals[]{new TestEquals(1),new TestEquals(3)};
        assertArrayEquals(array,set.toArray());
    }

    @Test
    public void testAdd() throws Exception {
        Set set = new EqualSet();
        assertTrue(set.add(first));
        assertFalse(set.add(equalToFirst));
        assertTrue(set.add(second));
    }

    @Test
    public void testRemove() throws Exception {
        Set set = new EqualSet();
        set.add(first);
        set.add(second);
        TestEquals array[] = new TestEquals[]{new TestEquals(1),new TestEquals(3)};
        assertArrayEquals(array,set.toArray());

        set.remove(equalToFirst);
        array = new TestEquals[]{second};
        assertArrayEquals(array,set.toArray());
    }

    @Test
    public void testContainsAll() throws Exception {
        Set set = new EqualSet();
        set.add(first);
        set.add(second);

        List list = new ArrayList<Object>();
        list.add(second);
        list.add(equalToFirst);
        Assert.assertTrue(set.containsAll(list));
    }

    @Test
    public void testAddAll() throws Exception {
        Set set = new EqualSet();
        set.add(first);

        List list = new ArrayList<Object>();
        list.add(equalToFirst);
        list.add(second);

        set.addAll(list);
        assertArrayEquals(list.toArray(),set.toArray());
    }

    @Test
    public void testRetainAll() throws Exception {
        Set set = new EqualSet();
        set.add(first);
        set.add(null);
        set.add(second);

        List list = new ArrayList<Object>();
        list.add(equalToFirst);
        list.add(second);
        set.retainAll(list);
        assertArrayEquals(list.toArray(), set.toArray());
    }

    @Test
    public void testRemoveAll() throws Exception {
        Set set = new EqualSet();
        set.add(first);
        set.add(null);
        set.add(second);

        List list = new ArrayList<Object>();
        list.add(equalToFirst);
        list.add(second);
        set.removeAll(list);
        list.clear();
        list.add(null);

        assertArrayEquals(list.toArray(), set.toArray());

    }

    @Test
    public void testClear() throws Exception {
        Set set = new EqualSet();
        set.add(first);
        set.add(second);
        set.clear();

        Assert.assertTrue(set.isEmpty());
    }

    @Test
    public void complexTest() throws Exception {
        Set set = new EqualSet();
        set.add(first);
        set.add(equalToFirst);
        set.add(second);

        TestEquals array[] = new TestEquals[]{new TestEquals(1),new TestEquals(3)};
        assertArrayEquals(array,set.toArray());

        set.add(null);
        array = new TestEquals[]{new TestEquals(1), new TestEquals(3),null};

        assertArrayEquals(array,set.toArray());
        assertFalse(set.add(null));
        assertArrayEquals(array,set.toArray());
    }
}

class TestEquals {
    int num;

    TestEquals(int val) {
        num = val;
    }

    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == this.getClass()) {
            TestEquals that = (TestEquals) obj;
            return that.num == this.num;
        }
        return false;
    }
}
