package com.luxoft.dnepr.courses.toprank;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

public class TopRankSearcher {
    private static final double DEFAULT_DAMPING_FACTOR = 0.8;
    private static final int DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING = 10;

    private String getPageContent(String link) {
        StringBuilder content = new StringBuilder("");

        System.setProperty("java.net.useSystemProxies", "true");
        try {
            URL url = URI.create(link).toURL();
            URLConnection connection;
            connection = url.openConnection();
            connection.connect();
            try (Scanner scanner = new Scanner(connection.getInputStream())) {
                while (scanner.hasNextLine()) {
                    content.append(scanner.nextLine());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content.toString();

    }

    private Map<String, String> getContent(List<String> urls) {
        final Map<String, String> urlContent = new ConcurrentHashMap<>();
        Thread[] threads = new Thread[urls.size()];
        int i = 0;
        for (final String url : urls) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    urlContent.put(url, getPageContent(url));
                }
            });
            threads[i++].start();
        }
        try {
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return urlContent;
    }

    public TopRankResults execute(List<String> urls) {
        TopRankExecutor topRankExecutor = new TopRankExecutor(DEFAULT_DAMPING_FACTOR, DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING);

        return topRankExecutor.execute(getContent(urls));
    }

    public TopRankResults execute(List<String> urls, double dampingFactor, int numberOfLoopsInRankComputing) {
        TopRankExecutor topRankExecutor = new TopRankExecutor(dampingFactor, numberOfLoopsInRankComputing);

        return topRankExecutor.execute(getContent(urls));
    }
}