package com.luxoft.courses.javaWeb.listeners;

import com.luxoft.courses.javaWeb.Constants;
import com.luxoft.courses.javaWeb.CurrentStatistics;
import com.luxoft.courses.javaWeb.Users;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;

public class SessionAttributeListener implements HttpSessionAttributeListener {
    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        Users users =(Users) event.getSession().getServletContext().getAttribute(Constants.USERS_STORAGE_ATTRIBUTE);
        String login = event.getSession().getAttribute(Constants.USER_LOGIN_ATTRIBUTE).toString();

        if (users.checkAdminAccessFor(login)) {
            getCurrentStatistics(event).getAdminSessions().incrementAndGet();
        } else {
            getCurrentStatistics(event).getUserSessions().incrementAndGet();
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        Users users =(Users) event.getSession().getServletContext().getAttribute(Constants.USERS_STORAGE_ATTRIBUTE);
        String login = event.getValue().toString();

        if (users.checkAdminAccessFor(login)) {
            getCurrentStatistics(event).getAdminSessions().getAndDecrement();
        } else {
            getCurrentStatistics(event).getUserSessions().getAndDecrement();
        }
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private CurrentStatistics getCurrentStatistics(HttpSessionEvent hse) {
        return (CurrentStatistics) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
    }


}