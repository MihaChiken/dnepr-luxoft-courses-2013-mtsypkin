SELECT DISTINCT
  makers.maker_name
FROM makers, product
WHERE makers.maker_id = product.maker_id AND
      product.type = 'PC' AND
      makers.maker_id NOT IN(
        SELECT makers.maker_id
        FROM  makers, product
        WHERE makers.maker_id = product.maker_id AND
              product.type = 'Laptop'
      )
