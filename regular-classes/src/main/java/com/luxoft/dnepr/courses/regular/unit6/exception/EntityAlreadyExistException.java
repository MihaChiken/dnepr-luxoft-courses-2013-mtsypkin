package com.luxoft.dnepr.courses.regular.unit6.exception;

public class EntityAlreadyExistException extends Exception {
    public EntityAlreadyExistException(String message){
        super(message);
    }
}
