<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/include/header.jsp"/>

<form action='${pageContext.request.contextPath}/getLogin' method='post'>
    Login:<br>
    <input type='text' name='login'><br>
    Password:<br>
    <input type='password' name='password'><br>
    <input type='submit' name='signIn' value='Sign in'>
</form>
<c:if test="${param.invalid==1}">
    <p class="error">Wrong login or password</p>
</c:if>

<jsp:include page="/WEB-INF/include/footer.jsp"/>