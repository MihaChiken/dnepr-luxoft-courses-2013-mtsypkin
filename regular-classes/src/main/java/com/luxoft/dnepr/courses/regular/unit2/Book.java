package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct implements Product {

    Date publicationDate;

    Book(String inpCode, String inpName, double inpPrice, Date inpDate) {
        setName(inpName);
        setCode(inpCode);
        setPrice(inpPrice);
        publicationDate = inpDate;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date inputDate) {
        publicationDate = inputDate;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode() + publicationDate.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass())
            return false;

        return super.equals(obj) && ((Book) obj).publicationDate == this.publicationDate;
    }
    @Override
    public Object clone() throws CloneNotSupportedException {
        Book obj = (Book) super.clone();
        obj.setPublicationDate(publicationDate);
        return obj;
    }
}
