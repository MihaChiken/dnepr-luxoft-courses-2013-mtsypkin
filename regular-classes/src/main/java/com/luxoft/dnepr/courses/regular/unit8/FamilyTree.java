package com.luxoft.dnepr.courses.regular.unit8;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FamilyTree implements Externalizable {
    
    private Person root;
    private transient Date creationTime;
    private static final long serialVersionUID = 1L;
    public FamilyTree() {
        setCreationTime(new Date());
    }

    public FamilyTree(Person root) {
        this();
        setRoot(root);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        if(root == null)
            root = new Person();
        root.readExternal(in);

    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {

        root.writeExternal(out);
        out.flush();
    }
    
    public Person getRoot() {
        return root;
    }
    
    public Date getCreationTime() {
        return creationTime;
    }
    
    private void setRoot(Person root) {
        this.root = root;
    }
    
    private void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
