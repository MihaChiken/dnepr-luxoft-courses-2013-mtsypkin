SELECT p.model, u.price
FROM makers AS m
  INNER JOIN product AS p ON m.maker_id = p.maker_id
  INNER JOIN (
               (SELECT model, price FROM printer)
               UNION
               (SELECT model, price FROM laptop)
               UNION
               (SELECT model, price FROM pc)
             ) AS u ON u.model = p.model
WHERE m.maker_name = 'B';