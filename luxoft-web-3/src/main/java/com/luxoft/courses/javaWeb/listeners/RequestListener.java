package com.luxoft.courses.javaWeb.listeners;

import com.luxoft.courses.javaWeb.Constants;
import com.luxoft.courses.javaWeb.CurrentStatistics;

import javax.servlet.*;
import javax.servlet.http.*;

public class RequestListener implements ServletRequestListener {

    @Override
    public void requestDestroyed(ServletRequestEvent event) {

    }

    @Override
    public void requestInitialized(ServletRequestEvent event) {
        HttpServletRequest request = (HttpServletRequest) event.getServletRequest();
        switch(request.getMethod()){
            case "GET":
                getCurrentStatistics(event).getGetRequests().incrementAndGet();
                break;
            case "POST":
                getCurrentStatistics(event).getPostRequests().incrementAndGet();
                break;
            default:
                getCurrentStatistics(event).getOtherRequests().incrementAndGet();
        }
    }
    private CurrentStatistics getCurrentStatistics(ServletRequestEvent event) {
        return (CurrentStatistics) event.getServletRequest().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
    }
}