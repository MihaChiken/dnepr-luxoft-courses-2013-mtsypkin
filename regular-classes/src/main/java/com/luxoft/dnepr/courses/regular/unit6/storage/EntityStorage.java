package com.luxoft.dnepr.courses.regular.unit6.storage;

import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.locks.*;

public class EntityStorage {

    private static final Map<Long, Entity> entities = new ConcurrentHashMap<>();

    private EntityStorage() {
    }

    public static Map<Long, Entity> getEntities() {
        return entities;
    }

    public static Long getNextId() {
        long maxId = 0;
            for (Long id : entities.keySet()) {
                if (id > maxId)
                    maxId = id;
            }
        return maxId + 1;
    }

}