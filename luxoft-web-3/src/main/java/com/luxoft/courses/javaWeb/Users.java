package com.luxoft.courses.javaWeb;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.ArrayList;
import java.util.List;

public class Users {
    private static List<User> users = new ArrayList<>();

    public Users(String json) {
        Object obj = JSONValue.parse(json);
        JSONArray array = (JSONArray) obj;
        for (int i = 0; i < array.size(); i++) {
            JSONObject jsonObject = (JSONObject) array.get(i);
            users.add(new User(
                    (String) jsonObject.get("login"),
                    (String) jsonObject.get("password"),
                    (String) jsonObject.get("role"))
            );
        }
    }



    public boolean isValidPair(String login, String password) {
        for (User user : users) {
            if (user.isValidPair(login, password)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkAdminAccessFor(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login) && user.getRole().equals("admin")) {
                return true;
            }
        }
        return false;
    }
}

