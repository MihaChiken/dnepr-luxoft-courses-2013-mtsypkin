package com.luxoft.dnepr.courses.regular.unit8;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class Serializer {

    public static void serialize(File file, FamilyTree entity) {

        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(entity);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static FamilyTree deserialize(File file) {

        FamilyTree familyTree = new FamilyTree();
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            familyTree = (FamilyTree) ois.readObject();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return familyTree;
    }
}