SELECT
  product.model,
  printer.price
FROM product, printer
WHERE product.model = printer.model
ORDER BY printer.price DESC
LIMIT 2
