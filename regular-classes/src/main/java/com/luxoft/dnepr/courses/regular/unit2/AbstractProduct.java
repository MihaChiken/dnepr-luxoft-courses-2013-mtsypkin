package com.luxoft.dnepr.courses.regular.unit2;

public abstract class AbstractProduct implements Product, Cloneable {
    private String code;
    private String name;
    private double price;

    public String getCode() {
        return code;
    }
    public void setCode(String inputCode){
        code = inputCode;
    }
    public String getName() {
        return name;
    }
    public void setName(String inputName){
        name = inputName;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double inputPrice){
        price = inputPrice;
    }
    public void addPrice(double inputPrice){
        price += inputPrice;
    }
    @Override
    public boolean equals(Object obj){
        if(this == obj) {
            return true;
        }

        if(this.getClass() != obj.getClass()) {
            return false;
        }

        AbstractProduct that = (AbstractProduct) obj;
        return this.code.equals(that.getCode()) && this.name.equals(that.getName());
    }
    @Override
    public int hashCode(){
        int hash = 7;
        hash = 31 * hash + (code == null ? 0 : code.hashCode());
        hash = 31 * hash + (name == null ? 0 : name.hashCode());
        return hash;
    }
    @Override
    public Object clone() throws CloneNotSupportedException {
        AbstractProduct obj = (AbstractProduct) super.clone();
        obj.setCode(code);
        obj.setName(name);
        obj.setPrice(price);
        return obj;
    }
}
