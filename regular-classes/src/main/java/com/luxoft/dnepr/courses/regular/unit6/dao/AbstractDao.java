package com.luxoft.dnepr.courses.regular.unit6.dao;

import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;

import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class AbstractDao implements IDao {
    private ReentrantLock saveLock = new ReentrantLock();

    private static final Map<Long, Entity> entities = EntityStorage.getEntities();

    @Override
    public Entity save(Entity entity) throws EntityAlreadyExistException {

        synchronized (entities) {
            if (entity.getId() == null) {
                entity.setId(EntityStorage.getNextId());
            } else {
                if (entities.containsKey(entity.getId())) {
                    throw new EntityAlreadyExistException("User with the same id(" + entity.getId() + ") is already exist");
                }
            }
            entities.put(entity.getId(), entity);
        }
        return entity;
    }

    @Override
    public Entity update(Entity entity) throws EntityNotFoundException {
        synchronized (entities) {
            if (entity.getId() == null || !entities.containsKey(entity.getId())) {
                throw new EntityNotFoundException("Thare is no user with id:" + entity.getId());
            }
            EntityStorage.getEntities().put(entity.getId(), entity);
        }
        return entity;
    }

    @Override
    public Entity get(long id) {
        synchronized (entities) {
            if (!entities.containsKey(id)) {
                return null;
            }
            return entities.get(id);
        }
    }

    @Override
    public boolean delete(long id) {
        synchronized (entities) {
            if (!entities.containsKey(id)) {
                return false;
            }
            entities.remove(id);
            return true;
        }
    }
}
