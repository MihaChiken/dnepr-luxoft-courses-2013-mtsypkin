SELECT
  makers.maker_name,
  printer.price
FROM product, makers, printer
WHERE product.model = printer.model AND
      product.maker_id = makers.maker_id AND
      printer.color = 'y'
ORDER BY printer.price
LIMIT 1

