package com.luxoft.courses.javaWeb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class GetLogin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> users = getUsers(getServletContext().getInitParameter("users"));
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        if (users.containsKey(login)) {
            if (users.get(login).equals(password)) {
                HttpSession session = request.getSession();
                session.setAttribute("login", login);

                response.sendRedirect("/user");
            } else {
                response.sendRedirect("/index.html?invalid=password");
            }
        } else {
            response.sendRedirect("/index.html?invalid=login");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    private Map<String, String> getUsers(String JSONString) {
        JSONString = JSONString.substring(1, JSONString.length() - 1);
        String[] usersPairsArray = JSONString.split(",");
        Map<String, String> users = new HashMap<>();

        for (String pair : usersPairsArray) {
            String[] loginAndPassword = pair.split(":");
            String login = loginAndPassword[0].trim();
            login = login.substring(1, login.length() - 1);
            String password = loginAndPassword[1].trim();
            password = password.substring(1, password.length() - 1);

            users.put(login, password);
        }
        return users;
    }

}
