package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    List<CompositeProduct> bill;

    Bill() {
        bill = new ArrayList<CompositeProduct>();
    }

    private boolean productsEquals(Product x, Product y) {
        if (x.getClass().getName().equals(y.getClass().getName())) {
            return x.equals(y);
        }
        return false;
    }

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {

        for (CompositeProduct composite : bill) {
            if (productsEquals(composite.getFirst(), product)) {
                composite.add(product);
                return;
            }
        }
        bill.add(new CompositeProduct(product));

    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double totalPrice = 0;
        for (CompositeProduct composite : bill) {
            totalPrice += composite.getPrice();
        }
        return totalPrice;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
        if (bill.size() == 0)
            return new ArrayList<>();


        List<Product> products = new ArrayList<Product>(bill);
        Comparator<Product> comparator = new ProductsComparator();
        Collections.sort(products, comparator);
        return products;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }
}

class ProductsComparator implements Comparator<Product> {
    @Override
    public int compare(Product x, Product y) {
        if (x.getPrice() < y.getPrice()) {
            return 1;
        }
        if (x.getPrice() > y.getPrice()) {
            return -1;
        }
        return 0;
    }
}
