package com.luxoft.dnepr.courses.regular.unit6;

import com.luxoft.dnepr.courses.regular.unit6.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit6.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.model.Employee;
import com.luxoft.dnepr.courses.regular.unit6.model.Redis;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class ThreadSaveTest {
    RedisDaoImpl<Redis> redisDao;
    EmployeeDaoImpl<Employee> employeeDao;

    @Test
    public void threadTest() throws InterruptedException {
        Thread thread;

        redisDao = new RedisDaoImpl<Redis>();
        employeeDao = new EmployeeDaoImpl<Employee>();
        for (int i = 0; i < 2000; i++) {
            thread = new Thread() {
                @Override
                public void run() {
                    try {
                        redisDao.save(new Redis());
                        employeeDao.save(new Employee());
                    } catch (EntityAlreadyExistException e) {
                        e.printStackTrace();
                    }

                }
            };
            thread.start();
        }
        TimeUnit.MILLISECONDS.sleep(3000);
        Assert.assertEquals(EntityStorage.getEntities().size(), 4000);
    }
}
